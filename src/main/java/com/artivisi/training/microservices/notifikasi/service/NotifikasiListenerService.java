package com.artivisi.training.microservices.notifikasi.service;

import com.artivisi.training.microservices.notifikasi.dto.Produk;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class NotifikasiListenerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotifikasiListenerService.class);

    @Autowired
    private ObjectMapper objectMapper;

    @KafkaListener(topics = "${topic.notification}")
    public void handleNotifikasiProdukBaru(String message){
        try{
            LOGGER.debug("Menerima message produk baru {}", message);
            Produk p = objectMapper.readValue(message, Produk.class);
            LOGGER.debug("Konversi json menjadi object Produk: {}", p);
        } catch(IOException e){
            LOGGER.error(e.getMessage(), e);
        }
    }
}
