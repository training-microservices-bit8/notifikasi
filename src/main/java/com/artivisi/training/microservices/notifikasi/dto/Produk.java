package com.artivisi.training.microservices.notifikasi.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Produk {
    private String id;
    private String kode;
    private String nama;
    private BigDecimal harga;
}
